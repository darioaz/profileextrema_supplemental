################
## Profile extrema for visualizing and quantifying uncertainties on excursion regions. Application to coastal flooding
## NOTE: this file is only intented as supplemental for the paper.
####
# Auxiliary file for Section4_3_1.R and Section4_3_2.R
# Creates the GP model for the coastal flooding test case
####
rm(list=ls())
# install and load the two required packages pGPx and profExtrema
# !!! This script only works with version 0.2.0 of profExtrema !!!
if (!requireNamespace("pGPx", quietly = TRUE)) {
  install.packages("pGPx_0.1.1.tar.gz",repos = NULL)
}
if (!requireNamespace("profExtrema", quietly = TRUE) | packageVersion("profExtrema")!='0.2.0') {
  install.packages("profExtrema_0.2.0.tar.gz",repos = NULL)
}
library(microbenchmark)
library(profExtrema)
library(DiceKriging)
library(DiceDesign)

# Create folders to save results
folderPlots<-"./CoastalFlood/plots/"
folderOuts<-"./CoastalFlood/out/"

if(!dir.exists(folderPlots)){
  dir.create(folderPlots,recursive=TRUE)
}
if(!dir.exists(folderOuts)){
  dir.create(folderOuts,recursive=TRUE)
}

# Define inputs
inputs<-data.frame(coastal_flooding[,-6])
colnames(inputs)<-colnames(coastal_flooding[,-6])
colnames(inputs)[4:5]<-c("tPlus","tMinus")

# put response in areaFlooded variable
areaFlooded<-data.frame(coastal_flooding[,6])
colnames(areaFlooded)<-colnames(coastal_flooding)[6]
## apply square-root transformation to output
response = sqrt(areaFlooded)

if(file.exists(paste(folderOuts,"GPmodel5d.RData",sep=""))){
  load(paste(folderOuts,"GPmodel5d.RData",sep=""))
}else{
  # generate initial model
  model <- km(formula=~Tide+Surge+I(phi^2)+tMinus+tPlus, design = inputs,response = response,covtype="matern3_2")

  loo_mat32_sqrtA<-leaveOneOut.km(model,type="UK")
  mse_loo_sqrtA<-sqrt(mean((loo_mat32_sqrtA$mean^2 -response$Area^2 )^2))

  Q2_coastal<-1-sum((loo_mat32_sqrtA$mean -response$Area )^2)/sum((response$Area-mean(response$Area))^2)

  # Plot initial model diagnostics
  cairo_pdf(paste(folderPlots,"diagnostics_GPmodel.pdf",sep=""),width = 12,height = 12)
  plot(model)
  dev.off()

  save(model,file=paste(folderOuts,"GPmodel5d.RData",sep=""))
}
object<-list(kmModel=model)
