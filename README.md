# README #

This repository contains the supplemental material for the editors and reviewers.

### The main files 

* `Section2_2.R` main file to reproduce the results on the analytical 2d function presented in section 2.2

* `Section3_3.R` main file to reproduce the GP emaulation results on the analytical 2d function presented in section 3.3

* `Section4_3_1.R` main file to reproduce the coordinate profile extrema results on the 5d coastal flooding test case. 

* `Section4_3_2.R` main file to reproduce the bivariate profile extrema results on the 5d coastal flooding test case. 

* `AppendixC*.R` files to reproduce the results shown in Appendix C, in particular Table 1 and Table 2.

* `AppendixE.R` main file to reproduce the results shown in Appendix E on the 3d analytical test function.


### The auxiliary packages

In order to run the code in the main files the following packages and their dependencies need to be installed

* pGPx\_0.1.1.tar.gz   R package for posterior quasi-realizations
* profExtrema\_0.2.0.tar.gz R package for profile extrema functions. It also contain the data for the coastal flooding test case. 


## References

Azzimonti, D., Ginsbourger, D., Rohmer, J. and Idier, D. (2017+). Profile extrema for visualizing and quantifying uncertainties on excursion regions. Application to coastal flooding. arXiv:1710.00688

Rohmer, J., Idier, D., Paris, F., Pedreros, R., and Louisor, J. (2018). Casting light on forcing and breaching scenarios that lead to marine inundation: Combining numerical simulations with a random-forest classification approach. _Environmental Modelling & Software_, 104:64-80.

Lazure, P. and Dumas, F. (2008). An external-internal mode coupling for a 3d hydrodynamical model for applications at regional scale (MARS). _Advances in Water Resources_, 31:233-250.
